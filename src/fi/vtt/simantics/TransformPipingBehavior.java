package fi.vtt.simantics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.util.IDGenerator;
import sapl.util.URITools;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Resource;

/* 
 * Version 07.05.2018
 */
public class TransformPipingBehavior extends ReusableAtomicBehavior {

	protected String query = null;
	protected String result = null;

	public static boolean DEBUG_PRINT = false;
	
	//physical connections from a pipe component
	Map<String, Set<String>> connections = new HashMap<String, Set<String>>();
	//physical connections from a pipe component to things external to the given PipeRun
	Map<String, Set<String>> externalConnections = new HashMap<String, Set<String>>(); 

	//Path segments corresponding to a pipe component (typically 1x1, but can be 2x1 or 1xN)
	Map<String, Set<String>> partSegments = new HashMap<String, Set<String>>();
	//Inverse of partsSegments
	Map<String, Set<String>> segmentParts = new HashMap<String, Set<String>>();

	//classes of path segments
	Map<String, String> segmentTypes = new HashMap<String, String>();
	//names of pipe components and ports, short-codes of path segments
	Map<String, String> names = new HashMap<String, String>();
	//ports used in connected things (like a nozzle of a tank) 
	Map<String, String> ports = new HashMap<String, String>();
	Map<String, Integer> portsNumber = new HashMap<String, Integer>();
	
	//segments representing pipe branching with links to a corresponding pipe port
	Map<String,String> branchPorts = new HashMap<String, String>();
	
	//start and end of segments
	Map<String, Double> sX = new HashMap<String, Double>();
	Map<String, Double> sY = new HashMap<String, Double>();
	Map<String, Double> sZ = new HashMap<String, Double>();
	Map<String, Double> eX = new HashMap<String, Double>();
	Map<String, Double> eY = new HashMap<String, Double>();
	Map<String, Double> eZ = new HashMap<String, Double>();
	Map<String, String> starts = new HashMap<String, String>();
	Map<String, String> ends = new HashMap<String, String>();
	Map<String, Set<String>> startsInv = new HashMap<String, Set<String>>();
	Map<String, Set<String>> endsInv = new HashMap<String, Set<String>>();
	
	
	//Working stuff
	List<String> toTrace = new ArrayList<String>();
	Set<String> alreadyTraced = new HashSet<String>();
	IDGenerator T_id = new IDGenerator("T");

	
	
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException{
		this.query = parameters.getObligatoryContainerID(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "query"));
		this.result = parameters.getObligatoryContainerID(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
	}

	protected void doAction() throws Exception {

		if(DEBUG_PRINT)
			System.out.println("Starting");
		
		SaplQueryResultSet rs = this.hasBeliefs(this.query, SaplConstants.G);

		if(rs != null){

			if(DEBUG_PRINT)
				System.out.println("Query executed: "+ rs.getNumberOfSolutions()+" solutions");

			//Process query solutions

			for(int i=0; i<rs.getNumberOfSolutions(); i++){
				Map<String, String> sol = rs.getSolution(i);

				String segment = sol.get("segment");
				String code = sol.get("code");
				String segmentType = sol.get("segmentType");
				String part = sol.get("part");
				String partName = sol.get("partName");
				String port = sol.get("port");
				String portsN = sol.get("portsN");
				
				String otherPart = sol.get("otherPart");
				String otherPartName = sol.get("otherPartName");
				String type = sol.get("type");
				String otherPort = sol.get("otherPort");
				String otherPortName = sol.get("otherPortName");
				String branchPort = sol.get("branchPort");

				names.put(part, partName);
				names.put(segment, code);
				names.put(otherPart, otherPartName);
				if(otherPortName!=null) names.put(otherPort, otherPortName);

				segmentTypes.put(segment, segmentType);
				ports.put(otherPart+" "+part, port);
				ports.put(part+" "+otherPart, otherPort);
				
				portsNumber.put(part, Integer.valueOf(portsN));

				if(branchPort!=null) branchPorts.put(segment, branchPort);
				
				Set<String> connected = connections.get(part);
				if(connected==null){
					connected = new HashSet<String>(2);
					connections.put(part,connected);
				}
				connected.add(otherPart);

				if(type!=null){
					Set<String> ext = externalConnections.get(part);
					if(ext==null){
						ext = new HashSet<String>(1);
						externalConnections.put(part, ext);
					}
					ext.add(otherPart);
				}

				Set<String> segs = partSegments.get(part);
				if(segs==null){
					segs = new HashSet<String>(2);
					partSegments.put(part,segs);
				}
				segs.add(segment);
				Set<String> parts = segmentParts.get(segment);
				if(parts==null){
					parts = new HashSet<String>(2);
					segmentParts.put(segment,parts);
				}
				parts.add(part);
				
				Double sx = Double.valueOf(sol.get("sx"));
				Double sy = Double.valueOf(sol.get("sy"));
				Double sz = Double.valueOf(sol.get("sz"));
				Double ex = Double.valueOf(sol.get("ex"));
				Double ey = Double.valueOf(sol.get("ey"));
				Double ez = Double.valueOf(sol.get("ez"));
				sX.put(segment, sx);
				sY.put(segment, sy);
				sZ.put(segment, sz);
				eX.put(segment, ex);
				eY.put(segment, ey);
				eZ.put(segment, ez);
				
				String start = sx+" "+sy+" "+sz;
				String end = ex+" "+ey+" "+ez;
				starts.put(segment, start);
				ends.put(segment, end);
				Set<String> ending = endsInv.get(end);
				if(ending==null){
					ending = new HashSet<String>(1);
					endsInv.put(end, ending);
				}
				ending.add(segment);
				Set<String> starting = startsInv.get(start);
				if(starting==null){
					starting = new HashSet<String>(1);
					startsInv.put(start, starting);
				}
				starting.add(segment);
			}

			//System.out.println(connections);
			//System.out.println(externalConnections);

			for(String part : connections.keySet()){
				Set<String> connected = connections.get(part);
				int internal = 0;
				Set<String> externalParts = new HashSet<String>();
				for(String c : connected){
					if(connections.keySet().contains(c)) internal++;
					else externalParts.add(c);
				}
				if(internal==1 && !externalParts.isEmpty()){
					for(String e : externalParts)
					toTrace.add(e+" "+part);
				}
			}
			
			print(segmentTypes.keySet().size()+" path segments");
			print(connections.keySet().size()+" parts");
			print(toTrace.size()+" external connections to trace");

			//Follow the pipes
			
			List<List<String>> result = new ArrayList<List<String>>();
			List<List<String>> resultConnections = new ArrayList<List<String>>();
			int count = 0;
			while(!toTrace.isEmpty()){
				String conn = toTrace.remove(0);
				String str[] = conn.split(" ");
				if(alreadyTraced.contains(conn) || alreadyTraced.contains(str[1]+" "+str[0])){
					if(DEBUG_PRINT)
						System.out.println("--------\nAlready traced "+conn);
				}
				else{
					count++;
					if(DEBUG_PRINT)
						System.out.println("\n\n----------------- "+count+" ----------------");
					List<List<String>> traceConnections = new ArrayList<List<String>>();
					result.addAll( tracePipe(str[0], str[1], rs, traceConnections) );
					resultConnections.addAll(traceConnections);
				}
			}
			
			if(DEBUG_PRINT) System.out.println();
			
			//Extend original solutions with division/ordering info
			Map<String, Map<String,String>> extensions = new HashMap<String,Map<String,String>>();

			Set<String> generatedComponents = new HashSet<String>();

			for(int i=0; i<result.size();i++){
				List<String> networkSegment = result.get(i);
				List<String> networkSegmentConn = resultConnections.get(i);
				Map<String,String> general = new HashMap<String,String>(4);
				general.put("edge", String.valueOf(i+1));
				general.put("from", getLocalName(networkSegmentConn.get(0)));
				general.put("to", getLocalName(networkSegmentConn.get(1)));
				Double minX=Double.MAX_VALUE, minY=Double.MAX_VALUE, minZ=Double.MAX_VALUE;
				Double maxX=-1*Double.MAX_VALUE, maxY=-1*Double.MIN_VALUE, maxZ=-1*Double.MIN_VALUE;

				int networkSegmentPart = 1;
				int points = 0;
				for(int j=0; j<networkSegment.size();j++){
					points++;
					
					Map<String,String> extension = new HashMap<String,String>();
					String segment = networkSegment.get(j);
					extensions.put(segment, extension);
					extension.put("order", String.valueOf(j+1));
					extension.put("subEdge", String.valueOf(networkSegmentPart));
					
					if(points==1){
						extension.put("edgeSX", String.valueOf(sX.get(segment)));
						extension.put("edgeSY", String.valueOf(sY.get(segment)));
						extension.put("edgeSZ", String.valueOf(sZ.get(segment)));
					}
					
					maxX = Math.max(maxX, Math.max(sX.get(segment), eX.get(segment)));
					maxY = Math.max(maxY, Math.max(sY.get(segment), eY.get(segment)));
					maxZ = Math.max(maxZ, Math.max(sZ.get(segment), eZ.get(segment)));
					minX = Math.min(minX, Math.min(sX.get(segment), eX.get(segment)));
					minY = Math.min(minY, Math.min(sY.get(segment), eY.get(segment)));
					minZ = Math.min(minZ, Math.min(sZ.get(segment), eZ.get(segment)));
					
					if(segmentTypes.get(segment).contains("AlongLegPath")){
						if(names.get(segment).toLowerCase().contains("valve")){
							
							String id = segmentParts.get(segment).iterator().next();
							extension.put("component", id);
							extension.put("componentLocalId", getLocalName(id));
							extension.put("componentUpperType", "PipingComponent");
							extension.put("componentType", "ShutOffValve");
							generatedComponents.add(id);

							for(int k=j; k>=0;k--){
								String earlier = networkSegment.get(k);
								Map<String,String> earlierExt = extensions.get(earlier);
								if(!earlierExt.get("subEdge").equals(String.valueOf(networkSegmentPart)))
									break;
								else earlierExt.put("points", String.valueOf(points+1));
							}
							
							networkSegmentPart++;
							points = 0;
						}
					}
					else if(segmentTypes.get(segment).contains("TurnPathFeat")){
						String id = segmentParts.get(segment).iterator().next();
						if(!generatedComponents.contains(id)){
							extension.put("component", id);
							extension.put("componentLocalId", getLocalName(id));
							extension.put("componentUpperType", "PipingComponent");
							extension.put("componentType", "PipingElbow");
							generatedComponents.add(id);
						}
					}
					else if(segmentTypes.get(segment).contains("StraightPathFeat")){
						String id = segmentParts.get(segment).iterator().next();
						if(!generatedComponents.contains(id)){
							
							extension.put("component", id);
							extension.put("componentLocalId", getLocalName(id));
							if(portsNumber.get(id) > 2){
								extension.put("componentUpperType", "PipingComponent");
								extension.put("componentType", "PipingTee");
							}
							else{
								extension.put("componentUpperType", "Pipe");
							}
							generatedComponents.add(id);
						}
					}
					
				}
				
				for(int k=networkSegment.size()-1; k>=0;k--){
					String earlier = networkSegment.get(k);
					Map<String,String> earlierExt = extensions.get(earlier);
					if(!earlierExt.get("subEdge").equals(String.valueOf(networkSegmentPart)))
						break;
					else earlierExt.put("points", String.valueOf(points+1));
				}

				general.put("edgeMaxX", String.valueOf(maxX));
				general.put("edgeMaxY", String.valueOf(maxY));
				general.put("edgeMaxZ", String.valueOf(maxZ));
				general.put("edgeMinX", String.valueOf(minX));
				general.put("edgeMinY", String.valueOf(minY));
				general.put("edgeMinZ", String.valueOf(minZ));
				for(int j=0; j<networkSegment.size();j++){
					String segment = networkSegment.get(j);
					Map<String,String> extension = extensions.get(segment);
					extension.putAll(general);
				}
			}
			
			for(int i=0; i<rs.getNumberOfSolutions(); i++){
				Map<String, String> sol = rs.getSolution(i);
				String segment = sol.get("segment");
				Map<String,String> extension = extensions.get(segment);
				if(extension!=null)
					sol.putAll(extension);
				else 
					System.err.println(segment);
			}
			
			Set<String> newVars = new HashSet<String>();
			newVars.add("edge"); newVars.add("subEdge"); newVars.add("edgeSY"); newVars.add("edgeSY"); newVars.add("edgeSZ");
			newVars.add("edgeMinX"); newVars.add("edgeMinY"); newVars.add("edgeMinZ");
			newVars.add("edgeMaxX"); newVars.add("edgeMaxY"); newVars.add("edgeMaxZ");
			newVars.add("from"); newVars.add("to"); newVars.add("points"); newVars.add("order");
			newVars.add("component"); newVars.add("componentLocalId");  
			newVars.add("componentUpperType"); newVars.add("componentType");
			rs.addDefinedVars(newVars);

			print(result.size()+" resulting network segments");
			
			this.addBeliefsAsRuleOutput(this.result, rs);

			print("Piping produced");

		}
		else
			System.err.println("No solutions to query");
	}
	
	protected static String getLocalName(String v){
		int ind = v.indexOf("#");
		if (ind == -1) ind = v.lastIndexOf("/");
		if (ind == -1) return v;
		else return v.substring(ind + 1);
	}
	
	
	boolean addPathSegments(List<String> arranged, List<List<String>> path, List<List<String>> subpathConnections){
		List<String> subpath = path.get(path.size()-1);
		List<String> subpathConn = subpathConnections.get(subpathConnections.size()-1);
		boolean didSplit = false;
		for(String a : arranged){
			if(segmentTypes.get(a).contains("AlongLegPath")){
				subpath.add(a);
				subpath = new ArrayList<String>();
				path.add(subpath);
				
				String id = branchPorts.get(a);
				if(id==null) id = T_id.getNewID();
				subpathConn.add(id);
				subpathConn = new ArrayList<String>();
				subpathConnections.add(subpathConn);
				subpathConn.add(id);
				didSplit = true;
			}
			else subpath.add(a);
		}
		
		return didSplit;
	}
	
	List<List<String>> tracePipe(String from, String startPart, SaplQueryResultSet rs, List<List<String>> subpathConnections){
		List<List<String>> path = new ArrayList<List<String>>();
		List<String> subpath = new ArrayList<String>();
		path.add(subpath);
		List<String> pathParts = new ArrayList<String>();
		List<String> pathPartPorts = new ArrayList<String>();

		pathParts.add(from);
		pathParts.add(startPart);
		String fromPort = ports.get(startPart+" "+from);
		pathPartPorts.add(fromPort);
		pathPartPorts.add(ports.get(from+" "+startPart));

		List<String> subpathConn = new ArrayList<String>();
		subpathConnections.add(subpathConn);
		subpathConn.add(fromPort);
		
		alreadyTraced.add(from+" "+startPart);
		
		String curr = startPart;
		
		if(DEBUG_PRINT)
			System.out.println("From: "+names.get(from)+" ("+from+")");
		
		while(true){

			if(DEBUG_PRINT)
				System.out.println("Current: "+names.get(curr)+" ("+curr+")");

			Set<String> connected = connections.get(curr);
			Set<String> currSegments = partSegments.get(curr);
			
			String p = subpath.isEmpty() ? null : subpath.get(subpath.size()-1);

			if(currSegments!=null && !subpath.containsAll(currSegments)){
				if(currSegments.size()==1){
					subpath.addAll(currSegments);
				}
				else { // it is a pipe with a >1 segments - maybe a T, or maybe just a bent pipe
					List<String> arranged = arrangeSegments(currSegments, rs);
					if(DEBUG_PRINT)
						System.out.println("Arranged for "+curr+" as: "+arranged);

					if(p==null){
						if(addPathSegments(arranged, path, subpathConnections)){
							subpath = path.get(path.size()-1);
							subpathConn = subpathConnections.get(subpathConnections.size()-1);
						}
					}
					else {
						Double min = Double.MAX_VALUE;
						int minSeg = -1;
						for(int i=0; i<arranged.size(); i++){
							String a = arranged.get(i);
							double d = Math.abs(sX.get(a)-sX.get(p)) + Math.abs(sY.get(a)-sY.get(p)) + Math.abs(sZ.get(a)-sZ.get(p));  
							d = Math.min(d , Math.abs(sX.get(a)-eX.get(p)) + Math.abs(sY.get(a)-eY.get(p)) + Math.abs(sZ.get(a)-eZ.get(p)));
							if(d < min){
								min = d; minSeg = i;
							}
							
							if(i==arranged.size()-1){
								d = Math.abs(eX.get(a)-eX.get(p)) + Math.abs(eY.get(a)-eY.get(p)) + Math.abs(eZ.get(a)-eZ.get(p));  
								d = Math.min(d , Math.abs(eX.get(a)-sX.get(p)) + Math.abs(eY.get(a)-sY.get(p)) + Math.abs(eZ.get(a)-sZ.get(p)));
								if(d < min){
									min = d; minSeg = arranged.size();
								}
							}
						}
						
						if(DEBUG_PRINT)
							System.out.println("Min:" + minSeg+ " : "+min);
						
						if(minSeg == 0 || minSeg == arranged.size()){ //can continue
							if(minSeg==arranged.size()) Collections.reverse(arranged);
							if(addPathSegments(arranged, path, subpathConnections)){
								subpath = path.get(path.size()-1);
								subpathConn = subpathConnections.get(subpathConnections.size()-1);
							}
						}
						else{
							if(DEBUG_PRINT)
								System.out.println("Reached a T");
							break; // this run connects to a T in the middle -> stop here
						}
					}
				}
			}
			
//			if(prevSegment!=null && connected.size() > 2){
//				List<String> arranged = arrangeSegments(currSegments, rs, null);
//				String end1 = starts.get(arranged.get(0));
//				String end2 = ends.get(arranged.get(arranged.size()-1));
//				Set<String> s1 = startsInv.get(end1);
//				Set<String> s2 = startsInv.get(end2);
//				Set<String> e1 = endsInv.get(end1);
//				Set<String> e2 = endsInv.get(end2);
//				
//				System.out.println(end1);
//				System.out.println(end2);
//				System.out.println(s1);
//				System.out.println(s2);
//				System.out.println(e1);
//				System.out.println(e2);
//				
//				System.out.println(starts.get(prevSegment));
//				System.out.println(ends.get(prevSegment));
//				
//				if(!(s1!=null && s1.contains(prevSegment) || s2!=null && s2.contains(prevSegment) 
//						|| e1!=null && e1.contains(prevSegment) || e2!=null && e2.contains(prevSegment)))
//					break; // this run connects to a pipe in the middle -> stop here
//			}
			
			
//			if(currSegments!=null && !path.containsAll(currSegments)){
//				if(currSegments.size()>1){
//					path.addAll(arrangeSegments(currSegments, rs, p));
//				}
//				else path.addAll(currSegments);
//			}

			if(connected!=null && !connected.isEmpty()){

				connected = new HashSet<String>(connected);

				connected.removeAll(pathParts);

				if(!connected.isEmpty()){
					
					Set<String> connectedCopy = new HashSet<String>(connected);
					
					if(connected.size() > 1){
						Set<String> ext = externalConnections.get(curr);
						
						if(ext!=null){
							if(!ext.containsAll(connected)){
								connected.removeAll(ext);
							}
							else { // only external connections left, but they are several
								for(String e : connected){
									String extPortName = names.get(ports.get(curr+" "+e));
									if(extPortName!=null){ // prefer one going to a equipment nozzle
										connected.clear();
										connected.add(e);
										break;
									}
								}
							}								
						}

					}

					if(connected.size() > 1) 
						System.err.println(curr + " -> "+connected);

					String nextPart = connected.iterator().next(); //hoping to have just one

					//tracedParts.add(curr);
					
					pathParts.add(nextPart);
					String port = ports.get(curr+" "+nextPart);
					if(port!=null) pathPartPorts.add(port);
					else pathPartPorts.add("");
					
					String conn = curr+" "+nextPart;
					if(alreadyTraced.contains(conn)){
						if(DEBUG_PRINT)
							System.out.println("Already traced "+conn);
						break;
					}

					alreadyTraced.add(conn);

					//branch out tracing
					if(connectedCopy.size()>1){
						for(String c : connectedCopy){
							if(c.equals(nextPart)) continue;
							toTrace.add(curr+" "+c);
						}
					}

					curr = nextPart;
				}
				else break;
			}
			else break;
		}
		
		String toPort = ports.get(pathParts.get(pathParts.size()-2)+" "+pathParts.get(pathParts.size()-1));
		subpathConn.add(toPort);

		//Print out results

		if(DEBUG_PRINT){
			System.out.println("--------");
	
			for(List<String> sub : path){
				for(String segment : sub){
					System.out.println(segment+ " " +segmentTypes.get(segment) + " " + names.get(segment));
				}
				System.out.println();
			}
	
			System.out.println();
	
			for(int i=0; i<pathParts.size(); i++){
				String part = pathParts.get(i);
				if(i==0 || i==pathParts.size()-1)
					System.out.println(names.get(part) + " (" +part+") : "+ names.get(pathPartPorts.get(i)) + " (" +pathPartPorts.get(i)+") ");
				else System.out.println(names.get(part)+ " (" +part+")");
			}
			
			System.out.println();
			
			//System.out.println(subpathConnections);
			//System.out.println();
			
		}
		
		return path;
		
	}


	private List<String> arrangeSegments(Set<String> segments, SaplQueryResultSet rs) {

		List<String> result = new ArrayList<String>();
		result.addAll(segments);

		int size = result.size();

		while(true){
			boolean done = true;
			for(int i=0; i<size; i++){
				String s = result.get(i);
				Set<String> prevSegments = endsInv.get(starts.get(s));
				
				if(prevSegments!=null){
					String target = null;
					
					for(String prevSegment : prevSegments){
						if(prevSegment.equals(s)) continue; // is so for zero-length segments
						int prevInd = result.indexOf(prevSegment);
						if(prevInd == -1) continue; //a segment of another pipe connecting to here 
						if(prevInd == i-1){
							target = null; break;
						}
						else target = prevSegment;
					}
					if(target!=null){
						result.remove(i);
						//System.out.println("Moving "+s+" to after "+target);
						int prevInd = result.indexOf(target);
						result.add(prevInd+1, s);
						done = false;
						break;
					}
				}
			}
			if(done) break;
		}

		return result;
	}
}
