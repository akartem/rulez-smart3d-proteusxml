package fi.vtt.simantics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/** 
 * @author Artem Katasonov (VTT)
 * Version 18.05.2018
 */
public class Smart3DModel {
	
	static public boolean PRINT_NAMES = true;
	static public boolean PRINT_DETAILS = false;

	static final String MODEL_HOST = "loinen.ad.vtt.fi:1433";

	static final String MODEL_DB = "ER_plant1_MDB";
	static final String SCHEMA_DB = "rulez_catalog_SCHEMA";

	//static final String ROOT_ID = "00009C45-0000-0000-0800-0234205A0708"; //Plant
	
	//static final String ROOT_ID = "0003345A-0000-0000-0900-3037205A1F04"; //Example Area
	
	static final String ROOT_ID ="0003345A-0000-0000-0000-A89D965A1F04"; //Aalto
	
	
	static final String fNEWLINE = System.getProperty("line.separator");
	
	static Set<String> skipProperties = new HashSet<String>();
	static {
		skipProperties.add("FromFMSiteToOcc");
		skipProperties.add("FromFlavorMgrtoFlavors");
		skipProperties.add("SystemsSpecs");
		skipProperties.add("ProxyOwner");

		//Equipment object properties
//		skipProperties.add("AssemblyMembersR");
//		skipProperties.add("DistribPorts");
//		skipProperties.add("HasPorts");
//		skipProperties.add("HasControlPoints");
//		skipProperties.add("HasCSystem");
//		skipProperties.add("HasShapes");
		
		//Pipeline object properties
		//skipProperties.add("OwnsParts"); 
		//skipProperties.add("OwnsDistributionConnection");
		//skipProperties.add("OwnsLogicalConnection");
		//skipProperties.add("PathSpecification");
	}
	
//	static final Set<String> TEST_OIDS = new HashSet<String>();
//	static{
//		TEST_OIDS.add("00004E2E-0000-0000-1B00-3037205A1F04"); //Tank1
//		TEST_OIDS.add("00004E23-0000-0000-7800-3037205A1F04"); //Nozzle H1
//	}
	
	//static final String TEST_OID ="00009C45-0000-0000-0800-0234205A0708"; //Plant
	//static final String TEST_OID = "0003345A-0000-0000-0900-3037205A1F04"; //Area
	
	//static final String TEST_OID = "00033454-0000-0000-0D00-3037205A1F04"; //ES
	static final String TEST_OID =  "00033455-0000-0000-1100-3037205A1F04"; //PS
	//static final String TEST_OID =  "00033457-0000-0000-1600-3037205A1F04"; //Pipeline
	
	//static final String TEST_OID = "00004E2E-0000-0000-1B00-3037205A1F04"; //Tank1
	//static final String TEST_OID = "00004E23-0000-0000-7800-3037205A1F04"; //Nozzle H1
	//static final String TEST_OID = "00004E23-0000-0000-6600-3037205A1F04"; //side Nozzle A2
	//static final String TEST_OID = "00004E23-0000-0000-A800-3037205A1F04"; //top Nozzle V
	//static final String TEST_OID = "00033457-0000-0000-1600-3037205A1F04"; //Pipe System PL1
	//static final String TEST_OID = "00004E27-0000-0000-4F00-3037205A1F04"; //Shape DP1

	
	static void addProperty(Map<String,Set<String>> properties, String property, String value){
		if(property==null || value==null) return;
		Set<String> values = properties.get(property);
		if(values==null){
			values = new LinkedHashSet<String>(1);
			properties.put(property, values);
		}
		values.add(value);
	}

	static void merge(Map<String,Set<String>> properties, Map<String,Set<String>> toAdd){
		for(String key : toAdd.keySet()){
			Set<String> values = properties.get(key);
			if(values==null) properties.put(key, toAdd.get(key));
			else values.addAll(toAdd.get(key));
		}
	}
	
	
	public static Map<String,Set<String>> readObjectName(String objectID, Connection connection){
		String selectSql = "SELECT * FROM dbo.CORENamedItem";  
		selectSql += " WHERE oid='"+objectID+"'";

		Statement statement = null;  
		ResultSet resultSet = null;  

		Map<String,Set<String>> result = new HashMap<String,Set<String>>();

		try{
			statement = connection.createStatement();  
			resultSet = statement.executeQuery(selectSql);

			if (resultSet.next()) {
				ResultSetMetaData meta = resultSet.getMetaData();
				int RC = meta.getColumnCount();
				for (short iCol = 0; iCol < RC; iCol++) {
					String label = meta.getColumnName(iCol + 1);
					if(label.toLowerCase().endsWith("name")){
						String name = resultSet.getString(iCol + 1);
						if(name!=null){
							addProperty(result, "Name", name);
							if(PRINT_NAMES)
								System.out.println("readObjectName: processed name: "+name);
						}
					}
				}
			}

			return result;

		} catch (Exception e) {
			e.printStackTrace();

		}
		finally {
			if (resultSet != null) {
				try { resultSet.close(); } catch (SQLException e) {}
			}
			if (statement != null) {
				try { statement.close(); } catch (SQLException e) {}
			}
		}		

		return result;
	}

	public static Map<String,Set<String>> readObjectSpecificClass(String objectID, Connection connection){

		Map<String,Set<String>> result = new HashMap<String,Set<String>>();
		
		String selectSql = "SELECT * FROM "+MODEL_DB+".dbo.COREAttribute as a "
				+ "JOIN "+SCHEMA_DB+".dbo.IJNamedObject as no ON a.CLSID=no.oid ";
		selectSql += " WHERE a.oid='"+objectID+"'";
	
		
		Statement statement = null;  
		ResultSet resultSet = null;  

		try{
			statement = connection.createStatement();  
			resultSet = statement.executeQuery(selectSql);

			if (resultSet.next()) {
				ResultSetMetaData meta = resultSet.getMetaData();
				int RC = meta.getColumnCount();
				for (short iCol = 0; iCol < RC; iCol++) {
					String label = meta.getColumnName(iCol + 1);
					if(label.toLowerCase().equals("name")){
						addProperty(result, "Type", resultSet.getString(iCol + 1));
					}
				}

				if(PRINT_DETAILS)
					System.out.println("readObjectClass: processed specific type");
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (resultSet != null) {
				try { resultSet.close(); } catch (SQLException e) {}
			}
			if (statement != null) {
				try { statement.close(); } catch (SQLException e) {}
			}
		}
		
		return result;
	}

	
	public static Map<String,Set<String>> readObjectInterfaceAttributes(String objectID, Connection connection){
		String[] attrTypes = {"Double", "Long", "Float", "Int", "Bool", "Bstr", "Date"};

		Map<String,Set<String>> result = new HashMap<String,Set<String>>();

		for(String type : attrTypes){
			String selectSql = "SELECT * FROM "+MODEL_DB+".dbo.CORE"+type+"Attribute as a "
					+ "JOIN "+SCHEMA_DB+".dbo.JInterface_Has_JMembers as jm ON a.iid=jm.oidOrg "
					+ "JOIN "+SCHEMA_DB+".dbo.IJInterfaceMember as m ON jm.oidDst=m.oid AND a.dispid=m.DispatchID "
					+ "JOIN "+SCHEMA_DB+".dbo.IJDBColumn as c ON jm.oidDst=c.oid";  
			selectSql += " WHERE a.oid='"+objectID+"'";

			Statement statement = null;  
			ResultSet resultSet = null;  

			try{
				statement = connection.createStatement();  
				resultSet = statement.executeQuery(selectSql);

				ResultSetMetaData meta = resultSet.getMetaData();
				int RC = meta.getColumnCount();
				int nameIdx = -1, valueIdx = -1;
				for (short iCol = 0; iCol < RC; iCol++) {
					String label = meta.getColumnName(iCol + 1);
					if(label.toLowerCase().endsWith("name")) nameIdx = iCol;
					else if(label.toLowerCase().endsWith("value")) valueIdx = iCol;
				}			

				int c = 0;
				while (resultSet.next()) {  
					c++;

					String name = resultSet.getString(nameIdx + 1);
					String value = resultSet.getString(valueIdx + 1);

					addProperty(result, name, value);
				} 				

				if(c!=0)
					if(PRINT_DETAILS)
						System.out.println("readObjectInterfaceAttributes: processed " + c + " "+ type +" attributes");

			} catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				if (resultSet != null) {
					try { resultSet.close(); } catch (SQLException e) {}
				}
				if (statement != null) {
					try { statement.close(); } catch (SQLException e) {}
				}
			}
		}

		return result;
	}

	public static Map<String,Set<String>> readObjectBaseClassAttributes(String objectID, Connection connection){

		Map<String,Set<String>> result = new HashMap<String,Set<String>>();

		String selectSql = "SELECT * FROM dbo.COREBaseClass as b JOIN dbo.COREJPOSchema as s ON b.ClassId=s.class_id";  
		selectSql += " WHERE oid='"+objectID+"'";

		Statement statement = null;  
		ResultSet resultSet = null;  
		Statement statement2 = null;  
		ResultSet resultSet2 = null;  

		try{
			statement = connection.createStatement();  
			resultSet = statement.executeQuery(selectSql);

			ResultSetMetaData meta = resultSet.getMetaData();
			int RC = meta.getColumnCount();
			int nameIdx = -1;
			for (short iCol = 0; iCol < RC; iCol++) {
				String label = meta.getColumnName(iCol + 1);
				if(label.toLowerCase().endsWith("tablename")) nameIdx = iCol;
			}			

			if (resultSet.next()) {  
				String type = resultSet.getString(nameIdx + 1);
				if(!type.trim().isEmpty()){
					addProperty(result, "Type", type);
	
					if(PRINT_DETAILS)
						System.out.println("readObjectBaseClassAttributes: processed base type: "+type);
	
					selectSql = "SELECT * FROM dbo."+type;  
					selectSql += " WHERE oid='"+objectID+"'";
	
					statement2 = connection.createStatement();  
					try{
						resultSet2 = statement2.executeQuery(selectSql);
	
						meta = resultSet2.getMetaData();
						RC = meta.getColumnCount();
						String labels[] = new String[RC];
						for (short iCol = 0; iCol < RC; iCol++) {
							labels[iCol] = meta.getColumnName(iCol + 1);
						}			
	
						int c = 0;
						if (resultSet2.next()) {  
							for (int iCol = 0; iCol < RC; iCol++) {
								String value = resultSet2.getString(iCol + 1);
								if(!labels[iCol].equals("oid") && value!=null && !value.equals("-1")){
									c++;
									addProperty(result, labels[iCol], value);
								}
							}
							if(PRINT_DETAILS)
								System.out.println("readObjectBaseClassAttributes: processed " + c + " base attributes");
						}
					}
					catch(SQLException e){
						System.out.println(e.getMessage());
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (resultSet != null) {
				try { resultSet.close(); } catch (SQLException e) {}
			}
			if (statement != null) {
				try { statement.close(); } catch (SQLException e) {}
			}
			if (resultSet2 != null) {
				try { resultSet2.close(); } catch (SQLException e) {}
			}
			if (statement2 != null) {
				try { statement2.close(); } catch (SQLException e) {}
			}
		}

		return result;

	}

	public static Map<String,Set<String>> readObjectSpatialAttributes(String objectID, Connection connection){
		Statement statement = null;  
		ResultSet resultSet = null;

		Map<String,Set<String>> result = new HashMap<String,Set<String>>();

		try{
			String selectSql = "SELECT * FROM dbo.CORESpatialIndex";  
			selectSql += " WHERE oid='"+objectID+"'";

			statement = connection.createStatement();  
			resultSet = statement.executeQuery(selectSql);

			ResultSetMetaData meta = resultSet.getMetaData();
			int RC = meta.getColumnCount();
			String labels[] = new String[RC];
			for (short iCol = 0; iCol < RC; iCol++) {
				labels[iCol] = meta.getColumnName(iCol + 1);
			}			

			int c = 0;
			if (resultSet.next()) {  
				for (int iCol = 0; iCol < RC; iCol++) {
					String value = resultSet.getString(iCol + 1);
					if(!labels[iCol].equals("oid") && value!=null){
						c++;
						addProperty(result, labels[iCol], value);
					}
				}
				if(PRINT_DETAILS)
					System.out.println("readObjectSpatialAttributes: processed " + c + " attributes");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (resultSet != null) {
				try { resultSet.close(); } catch (SQLException e) {}
			}
			if (statement != null) {
				try { statement.close(); } catch (SQLException e) {}
			}
		}
		
		return result;
	}

	public static Map<String,Set<String>> readObjectSymbolAttributes(String objectID, Connection connection){
		Statement statement = null;  
		ResultSet resultSet = null;

		Map<String,Set<String>> result = new HashMap<String,Set<String>>();

		try{
			String selectSql = "SELECT * FROM dbo.CORESymbol";  
			selectSql += " WHERE oid='"+objectID+"'";

			statement = connection.createStatement();  
			resultSet = statement.executeQuery(selectSql);

			if (resultSet.next()) {
				addProperty(result, "East", resultSet.getString("ServerToClient12"));
				addProperty(result, "North", resultSet.getString("ServerToClient13"));
				addProperty(result, "Elevation", resultSet.getString("ServerToClient14"));
				
//				String flavor = resultSet.getString("flavorOid");
//				addProperty(result, "Flavor", flavor);
//				connectedObjects.add(flavor);
				
				if(PRINT_DETAILS)
					System.out.println("readObjectSymbolAttributes: processed 3 attributes");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (resultSet != null) {
				try { resultSet.close(); } catch (SQLException e) {}
			}
			if (statement != null) {
				try { statement.close(); } catch (SQLException e) {}
			}
		}
		
		return result;		
	}
	
	public static Map<String,Set<String>> readObjectRelations(String objectID, Connection connection, Set<String> connectedObjects){
		Statement statement = null;  
		ResultSet resultSet = null;

		Map<String,Set<String>> result = new HashMap<String,Set<String>>();
		
		try{
			String selectSql = "SELECT * FROM dbo.CORERelationOrigin as r "
					+ "LEFT JOIN dbo.CORENamedItem as n ON r.oidTarget=n.oid "
					+ "JOIN "+SCHEMA_DB+".dbo.IJNamedObject as rn ON r.RelationType=rn.oid";
			selectSql += " WHERE r.oid='"+objectID+"'";

			statement = connection.createStatement();  
			resultSet = statement.executeQuery(selectSql);

			int c = 0;
			while (resultSet.next()) {
				String target = resultSet.getString("oidTarget");
				//if(TEST_OIDS.contains(target)){
					String prop = resultSet.getString("Name");
					if(!skipProperties.contains(prop)){
						c++;
						addProperty(result, prop, target);
						connectedObjects.add(target);
					}
				//}
			}
			
			if(PRINT_DETAILS)
				System.out.println("readObjectRelations: processed "+c+" relations");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (resultSet != null) {
				try { resultSet.close(); } catch (SQLException e) {}
			}
			if (statement != null) {
				try { statement.close(); } catch (SQLException e) {}
			}
		}
		
		return result;
	}
	

	public static Map<String,Set<String>> readObject(String objectID, Connection connection, Set<String> connectedObjects){
		Map<String,Set<String>> result = new HashMap<String,Set<String>>();

		merge(result, readObjectName(objectID, connection));
		merge(result, readObjectBaseClassAttributes(objectID, connection));
		merge(result, readObjectSpecificClass(objectID, connection));
		merge(result, readObjectInterfaceAttributes(objectID, connection));
		merge(result, readObjectSpatialAttributes(objectID, connection));
		merge(result, readObjectSymbolAttributes(objectID, connection));

		merge(result, readObjectRelations(objectID, connection, connectedObjects));
		
		if(PRINT_DETAILS) System.out.println();
		
		return result;
	}

	public static Map<String, Map<String,Set<String>>> readModel(Set<String> oids){
		Connection connection = null; 		

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			String connectionUrl = "jdbc:sqlserver://"+MODEL_HOST+";" +  
					"databaseName="+MODEL_DB+";integratedSecurity=true";  

			connection = DriverManager.getConnection(connectionUrl); 		

			Map<String, Map<String,Set<String>>> result = new LinkedHashMap<String, Map<String,Set<String>>>(); 
			
//			for(String oid : TEST_OIDS){
//				Map<String,String> obj = readObject(oid, connection);
//				result.put(oid, obj);
//			}

			Set<String> toRead = new HashSet<String>();
			toRead.add(ROOT_ID);
			//toRead.add(TEST_OID);
			
			while(!toRead.isEmpty()){
				Set<String> connectedObjects = new HashSet<String>();
				for(String objToRead : toRead){
					oids.add(objToRead);
					Map<String,Set<String>> obj = readObject(objToRead, connection, connectedObjects);
					result.put(objToRead, obj);
				}
				connectedObjects.removeAll(oids); // keep only new
				toRead = connectedObjects;
			}
			
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			if (connection != null) {
				try { connection.close(); } catch (SQLException e) {}
			}
		}

	}

	public static String makeRDF(Map<String, Map<String,Set<String>>> model, Set<String> oids){
		StringBuilder result = new StringBuilder();
		String obj_ns = "ex:";
		String ns = "s3d:";
		
		result.append("@prefix "+ns+" <http://hexagonppm.com/intergraph-smart-3d/>."+fNEWLINE);
		result.append("@prefix "+obj_ns+" <http://example.org/>."+fNEWLINE);
		result.append(fNEWLINE);
		
		for(String oid : model.keySet()){
			
			Map<String,Set<String>> data = model.get(oid);

			result.append(obj_ns+oid);
			
			boolean firstProperty = true;
			
			Set<String> types = data.get("Type");
			if(types!=null){
				result.append(" a ");
				boolean first = true; 
				for(String type : types){
					if(!first) result.append(", ");
					result.append(ns+type);
					first = false;
					firstProperty = false;
				}
			}

			Set<String> names = data.get("Name");
			if(names!=null){
				if(types!=null) result.append(';');
				result.append(' ');
				result.append(ns+"Name ");
				boolean first = true; 
				for(String name : names){
					if(!first) result.append(", ");
					result.append("\""+name+"\"");
					first = false;
					firstProperty = false;
				}
			}
			
			List<String> keys = new ArrayList<String>(data.keySet());
			Collections.sort(keys);
			
			for(String key : keys){
				if(!key.equals("Name") && !key.equals("Type")){
					Set<String> values = data.get(key);
					for(String value : values){
						if(!firstProperty) result.append(';');
						result.append(fNEWLINE);
						result.append("    ");
						if(oids.contains(value)) value=obj_ns+value;
						else {
							try{
								Double.valueOf(value);
							}
							catch(NumberFormatException e){
								value = "\""+value+"\"";
							}
						}
						 
						result.append(ns+key+" "+value);
						firstProperty = false;
						
					}
					
				}
			}
			
			result.append("."+fNEWLINE+fNEWLINE);
		}
		
		return result.toString();
	}

	public static boolean writeToFile(String filename, String text, boolean append, String encoding){
		BufferedWriter ff = null;
		OutputStreamWriter fwrite = null;
		try {
			File datafile = new File(filename);
			File p = datafile.getParentFile();
			if ((p != null) && !p.exists()) {
				boolean directoryMakingSuccessFul = p.mkdirs();
				if (!directoryMakingSuccessFul) {
					System.err.println("Failed creating necessary directories : " + p.getCanonicalPath());
					return false;
				}
			}
			if(encoding==null){
				fwrite = new FileWriter(filename, append);
			}
			else {
				fwrite = new OutputStreamWriter( new FileOutputStream(filename), encoding);
			}
			ff = new BufferedWriter(fwrite);

			ff.write(text, 0, text.length());
			ff.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (ff != null) {
				try {
					ff.close();
				} catch (IOException e) {
					// not much we can do here.
				}
			} else {
				//try closing FileWriter at least
				if (fwrite != null) {
					try {
						fwrite.close();
					} catch (IOException e) {
						// not much we can do here.
					}
				}
			}
		}
	}
	
	public static void main(String[] args) {
		Set<String> oids = new HashSet<String>();
		
		long start = System.currentTimeMillis();
		Map<String, Map<String,Set<String>>> r = readModel(oids);
		long read = System.currentTimeMillis();
		//System.out.println(r);
		String rdf = makeRDF(r, oids);
		//System.out.println(rdf);
		writeToFile("model.ttl", rdf, false, null);
		long write = System.currentTimeMillis();
		
		System.out.println("DONE ("+(read-start)+" + "+(write-read)+")");
	}

}
