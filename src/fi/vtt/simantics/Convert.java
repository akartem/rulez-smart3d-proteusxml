package fi.vtt.simantics;

import java.io.IOException;

import sapl.Sapl;
import sapl.core.SaplAgent;
import sapl.core.SaplReasoner;

public class Convert {

	static String[] scripts = {
		"convert.sapl",
	};

	public static void main(String[] args) throws IOException, InterruptedException {
		
		SaplReasoner.PERFORMANCE_CYCLE_PRINT = false;
		SaplReasoner.SLEEP_PRINT = false;
		SaplAgent.SLEEP_PRINT = false;
		SaplAgent.STOP_PRINT = false;
		
		//run
		Sapl.execute("test", scripts);
	}

}
